package rdt;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.TimerTask;

class TimeoutHandler extends TimerTask {
	RDTBuffer sndBuf;
	RDTSegment seg; 
	DatagramSocket socket;
	InetAddress ip;
	int port;
	
	TimeoutHandler (RDTBuffer sndBuf_, RDTSegment s, DatagramSocket sock, InetAddress ip_addr, int p) {
		sndBuf = sndBuf_;
		seg = s;
		socket = sock;
		ip = ip_addr;
		port = p;
	}
	public void run() {
		
		System.out.println(System.currentTimeMillis()+ ":Timeout for seg: " + seg.seqNum);
		System.out.flush();
		
		switch(RDT.protocol){
			case RDT.GBN:
				/*
				 * send packets within rcvWin
				 * timer set for the oldest unacked packet
				 * wait for acks until all packets in the receiving window have arrived
				 * if timer expires before all acks in rcvWin arrived, resend all packets in rcvWin
				 * otherwise, if all acks received before timeout, stop the timer,
				 * 	move the window, and set timer for new base
				*/
				long startTime;
				boolean continueSending = true;
				while(!seg.ackReceived){ //While we have yet to recieve ack for seg
					startTime = System.currentTimeMillis();   //start clock

					while (System.currentTimeMillis() - startTime < RDT.RTO){
						if(seg.ackReceived){
							try {
								sndBuf.semEmpty.acquire(); // wait for an empty slot 
								sndBuf.semMutex.acquire(); // wait for mutex 
								sndBuf.base++;
								sndBuf.next++;
								sndBuf.semMutex.release();
								sndBuf.semFull.release(); // increase #of full slots
							} catch(InterruptedException e) {
								System.out.println("increment window position: " + e);
							}
							continueSending = false;
							break;
						}
					}
					if(!continueSending){
						break;
					}
					if (System.currentTimeMillis() - startTime >= RDT.RTO && !seg.ackReceived)
					{
						Utility.udp_send(seg, socket, ip, port);	
					}
				}
				break;

			case RDT.SR:
				
				break;
			default:
				System.out.println("Error in TimeoutHandler:run(): unknown protocol");
		}
		
	}
} // end TimeoutHandler class

