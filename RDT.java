package rdt;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.Arrays;

public class RDT {

	public static final int MSS = 100; // Max segement size in bytes
	public static final int RTO = 500; // Retransmission Timeout in msec
	public static final int ERROR = -1;
	public static final int MAX_BUF_SIZE = 3;  
	public static final int GBN = 1;   // Go back N protocol
	public static final int SR = 2;    // Selective Repeat
	public static final int protocol = GBN;
	
	public static double lossRate = 0.0;
	public static Random random = new Random(); 
	public static Timer timer = new Timer();	
	
	private DatagramSocket socket; //Create a UDP datagram socket 
	private InetAddress dst_ip;
	private int dst_port;
	private int local_port; 
	
	private RDTBuffer sndBuf;
	private RDTBuffer rcvBuf;
	
	private ReceiverThread rcvThread;  
	
	//Constructor for RDT object, takes in a destination hostname, port#, source port#. No rcv buffer version 
	RDT (String dst_hostname_, int dst_port_, int local_port_) 
	{
		local_port = local_port_;
		dst_port = dst_port_; 
		try {
			 socket = new DatagramSocket(local_port);
			 dst_ip = InetAddress.getByName(dst_hostname_);
		 } 
		 catch (IOException e) {
			System.out.println("RDT constructor: " + e);
		 }
		sndBuf = new RDTBuffer(MAX_BUF_SIZE);
		if (protocol == GBN){
			rcvBuf = new RDTBuffer(1);
		}
		else {	//Used for Selective Repeat 
			rcvBuf = new RDTBuffer(MAX_BUF_SIZE);
		}
		rcvThread = new ReceiverThread(rcvBuf, sndBuf, socket, dst_ip, dst_port);
		rcvThread.start();
	}
	
	RDT (String dst_hostname_, int dst_port_, int local_port_, int sndBufSize, int rcvBufSize) 
	{
		local_port = local_port_;
		dst_port = dst_port_;
		try {
			socket = new DatagramSocket(local_port);
			dst_ip = InetAddress.getByName(dst_hostname_);
		}catch (IOException e) {
			System.out.println("RDT constructor: " + e);
		}
		sndBuf = new RDTBuffer(sndBufSize);
		if (protocol == GBN){
			rcvBuf = new RDTBuffer(1); 
		}
		else {
			rcvBuf = new RDTBuffer(rcvBufSize);
		}
		
		rcvThread = new ReceiverThread(rcvBuf, sndBuf, socket, dst_ip, dst_port);
		rcvThread.start();
	}
	
	public static void setLossRate(double rate) {
		lossRate = rate;
	}
	
	// called by app, returns total number of sent bytes  
	public int send(byte[] app_data, int size) {
		
		//****** complete, size is the size of the data 
		// divide data into segments
		// put each segment into sndBuf
		// send using udp_send(), possibly using 
		// schedule timeout for segment(s) 
		//Assumption: when app calls function, send all the data to reciever even if it takes more than one segment

		int app_data_size = size; 
		int seg_size = RDT.MSS;
		
		int num_segments_send = (int)Math.ceil(app_data_size / seg_size);
		int num_segments_sent = 0;
		int currentSeg = 0;
		RDTSegment seg_to_send = null;

		RDTSegment[] array_of_seg = new RDTSegment[num_segments_send];

		if(num_segments_send <= 0){
			return 0;
		}
		else if(num_segments_send == 1){
			System.arraycopy(app_data, 0, array_of_seg[currentSeg].data, 0, size);
		}
		else{

			
			}
		}

		for(int i = 0; i < num_segments_send; i++){
			
			array_of_seg[i].length = array_of_seg[i].data.length;
			
			sndBuf.putNext(array_of_seg[i]);
			seg_to_send = sndBuf.getNext();
			Utility.udp_send(seg_to_send, socket, dst_ip, dst_port);
			num_segments_sent++;
		}


		return;
	}
	
	//called by app, receive one segment at a time and returns number of bytes copied in buf
	//Size is the max segment size, buffer is equal to size of segment 

	public int receive (byte[] buf, int size)
	{
		int bytesCopied = 0;
		ReceiverThread rcv_thread = new ReceiverThread(rcvBuf, sndBuf, socket, dst_ip, dst_port);
		rcv_thread.run();

		RDTSegment rcv_seg = null;

		while(rcvBuf.getNext() != null){
			rcv_seg = rcvBuf.getNext();
			System.arraycopy(rcv_seg.data, 0, buf, 0, size); //copy the segment data into buffer
			bytesCopied = bytesCopied + rcv_seg.data.length;
		}
		return bytesCopied;
	}
	
	// called by app
	public void close() {
		// OPTIONAL: close the connection gracefully
		// you can use TCP-style connection termination process
	}
	
}  // end RDT class 


class RDTBuffer {
	public RDTSegment[] buf;
	public int size;	
	public int base;
	public int next;
	public Semaphore semMutex; // for mutual execlusion
	public Semaphore semFull; // #of full slots
	public Semaphore semEmpty; // #of Empty slots
	
	//Constructor to be called by both sender, reciever buffers
	RDTBuffer (int bufSize) {
		buf = new RDTSegment[bufSize];
		for (int i=0; i<bufSize; i++){
			buf[i] = null;
		}
		size = bufSize;
		base = next = 0;

		//Semaphore(int permits, boolean fair) where fairness ensures no thread will be starved of execution time
		semMutex = new Semaphore(1, true);
		semFull =  new Semaphore(0, true);
		semEmpty = new Semaphore(bufSize, true);
	}

	// Put a segment in the next available slot in the buffer
	public void putNext(RDTSegment seg) {		
		try {
			semEmpty.acquire(); // wait for an empty slot 
			semMutex.acquire(); // wait for mutex 
				buf[next%size] = seg;
				next++;  
			semMutex.release(); //finished with critical section, release mutex lock 
			semFull.release(); // increase #of full slots
		}catch(InterruptedException e) {
			System.out.println("Buffer put(): " + e);
		}
	}
	
	/*
	Reason I chose buff[base%size] is because unless base == next, 
	next should always be pointing to an empty slot in the buffer. putNext()
	puts a segment into an empty slot.

	Example: buff with size = 6
	putNext() --> buff[0] filled, base = 0, next = 1 
	putNext() --> buff[1] filled, base = 0, next = 2
	getNext() --> buff[0] emptied, base = 1, next = 2
	getNext() --> buff[1] emptied, base = 2, next = 2, 

	buffer now emptied and getNext will just return null since buff is empty
	*/

	// return the next in-order segment
	public RDTSegment getNext() {
		RDTSegment next_Seg =  null; //getNext returns an RDTSegment object, put it into variable of type RDTSegment
		if(size != 0 && (base != next)){
			try {
					semEmpty.acquire(); // wait for an empty slot 
					semMutex.acquire(); // wait for mutex 
						next_Seg = buf[base%size];
						base++;
					semMutex.release(); //finished with critical section, release mutex lock 
					semFull.release(); // increase #of full slots
			}catch(InterruptedException e) {
				System.out.println("Buffer get(): " + e);
			}	
		}	
		return next_Seg;	
	}
	
	// Put a segment in the *right* slot based on seg.seqNum
	// used by receiver in Selective Repeat
	public void putSeqNum (RDTSegment seg) {
			try {
			semEmpty.acquire(); // wait for an empty slot 
			semMutex.acquire(); // wait for mutex 
				buf[seg.seqNum%size] = seg; 
			semMutex.release(); //finished with critical section, release mutex lock 
			semFull.release(); // increase #of full slots
		} catch(InterruptedException e) {
			System.out.println("Buffer put(): " + e);
		}
	}
	
	// for debugging
	public void dump() {
		System.out.println("Dumping the receiver buffer ...");
		for (int i=0; i<size; i++){
			System.out.println(buf[i]);
		}
		
	}
} // end RDTBuffer class


//The ReceiverThread class implements a thread that concurrently runs with the send thread. 
//An instance of the ReceiverThread continually waits on the socket to process the incoming data.
class ReceiverThread extends Thread {
	RDTBuffer rcvBuf, sndBuf;
	DatagramSocket socket;
	InetAddress dst_ip;
	int dst_port;
	
	ReceiverThread (RDTBuffer rcv_buf, RDTBuffer snd_buf, DatagramSocket s, InetAddress dst_ip_, int dst_port_) {
		rcvBuf = rcv_buf;
		sndBuf = snd_buf;
		socket = s;
		dst_ip = dst_ip_;
		dst_port = dst_port_;
	}	
	public void run() {
		// *** complete 
		// Essentially:  while(cond==true){  // may loop for ever if you will not implement RDT::close()  
		//                socket.receive(pkt)
		//                seg = make a segment from the pkt
		//                verify checksum of seg
		//	              if seg contains ACK, process it potentailly removing segments from sndBuf
		//                if seg contains data, put the data in rcvBuf and do any necessary 
		//                             stuff (e.g, send ACK)
		//

		//Cannot create a packet for socket using rcvBuf due to DatagramPacket requiring a byte[] arg
		byte[] middlemanRcvBuf = new byte[1024];
		byte[] middlemanSendBuf = new byte[1024];

		while(true){
			
			//Note: UDP datagrams on reciever side don't need to specify IP address, port number
			DatagramPacket recievePacket = new DatagramPacket(middlemanRcvBuf, middlemanRcvBuf.length);
			
			try{
				socket.receive(recievePacket);
			} catch(IOException e){
				System.out.println("Unable to recieve packet from socket " + e);
			}
		
			byte[] bytebuff = recievePacket.getData(); //need byte array to specifically to get data from socket

			RDTSegment seg = new RDTSegment();
			makeSegment(seg, bytebuff);

			if(seg.isValid()){

				if(seg.containsAck()){
					sndBuf.buf[seg.seqNum] = null; //nullify the slot occupied by a segment with seg.seqNum	
					
					if(rcvBuf.size == 1){
						rcvBuf.putNext(seg); //GBN the rcvBuff is size 1, put seg into any avalaible slot
					}
					else{
						rcvBuf.putSeqNum(seg); //place the seg with ACK into rcvBuff. App will eventually call RDT.recieve and pick it up
					} 
				}
				else if(seg.containsData()){  //put seg containing data into rcvBuff

					if(rcvBuf.size == 1){
						rcvBuf.putNext(seg);
					}
					else{
						rcvBuf.putSeqNum(seg);
					}
  
					RDTSegment seg_ack = new RDTSegment(); //Create an ACK segment with an ACK num = seg.seqNum
					seg_ack.ackNum = seg.seqNum;

					byte[] payload_ack = new byte[seg.length]; 
					middlemanSendBuf = seg.makePayload(payload_ack); 

					seg_ack.printHeader(); //debugging purposes
					seg_ack.printData();
					
					DatagramPacket sendPacket = new DatagramPacket(middlemanSendBuf, middlemanSendBuf.length, dst_ip, dst_port);
					try{
						socket.send(sendPacket);	
					} catch(IOException e){
						System.out.println("Error trying to send packet from socket to network " + e);
					}

					Arrays.fill(middlemanSendBuf, (byte)0);
					Arrays.fill(middlemanRcvBuf, (byte)0);
				}
			}
		}
	}
	
//	 create a segment from received bytes 
	void makeSegment(RDTSegment seg, byte[] payload) {
	
		seg.seqNum = Utility.byteToInt(payload, RDTSegment.SEQ_NUM_OFFSET);
		seg.ackNum = Utility.byteToInt(payload, RDTSegment.ACK_NUM_OFFSET);
		seg.flags  = Utility.byteToInt(payload, RDTSegment.FLAGS_OFFSET);
		seg.checksum = Utility.byteToInt(payload, RDTSegment.CHECKSUM_OFFSET);
		seg.rcvWin = Utility.byteToInt(payload, RDTSegment.RCV_WIN_OFFSET);
		seg.length = Utility.byteToInt(payload, RDTSegment.LENGTH_OFFSET);

		for (int i=0; i< seg.length; i++)
			seg.data[i] = payload[i + RDTSegment.HDR_SIZE]; 
	}
	
} // end ReceiverThread class

